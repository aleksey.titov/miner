<?php

/**
 * Class Miner
 */
class Miner
{
    const MINE_SIGN = 'x';

    const AROUND_INTERVAL = [-1, 0, 1];

    /**
     * @var array
     */
    private $board = [
        ['x', 'x', 'x', '0', '0'],
        ['0', 'x', '0', 'x', '0'],
        ['0', '0', 'x', 'x', '0'],
    ];

    /**
     * @var array
     */
    private $solvedBoard;

    public function __construct()
    {
        $this->solvedBoard = $this->mines($this->board);

    }

    /**
     * @param $board
     * @return array
     */
    private function mines($board): array
    {
        foreach ($board as $key => $line) {
            foreach ($line as $keyLine => $cell) {
                if ($cell !== self::MINE_SIGN) {
                    $board[$key][$keyLine] = $this->checkAndIncField($board, $key, $keyLine);
                }
            }
        }

        return $board;
    }

    /**
     * @return array
     */
    public function getSolvedBoard(): array
    {
        return $this->solvedBoard;
    }

    /**
     * @param array $board
     * @param int $key
     * @param int $keyLine
     * @return int
     */
    private function checkAndIncField(array $board, int $key, int $keyLine): int
    {
        $count = 0;
        foreach (self::AROUND_INTERVAL as $i) {
            foreach (self::AROUND_INTERVAL as $j) {
                if (
                    ($i != 0 || $j != 0)
                    && isset($board[$key + $i][$keyLine + $j])
                    && $board[$key + $i][$keyLine + $j] == self::MINE_SIGN
                ) {
                    $count++;
                }
            }
        }

        return $count;
    }

}

$miner = new Miner;

var_dump($miner->getSolvedBoard());